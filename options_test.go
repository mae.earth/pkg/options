/* mae.earth/pkg/options_test.go */
package options

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_Options(t *testing.T) {
	Convey("Options",t,func() {

		Convey("Token should be incorrect format",func() {
	
			ops,err := Parse("abc","foo")
			So(err,ShouldNotBeNil)
			So(ops,ShouldBeNil)
		})


		Convey("Parse",func() {

			ops,err := Parse("string abc","foo","int num",101,"float f",1.234)
			So(err,ShouldBeNil)
			So(ops,ShouldNotBeEmpty)
			So(len(ops),ShouldEqual,3)

			Convey("first option",func() {
				So(ops[0].Type,ShouldEqual,"string")
				So(ops[0].Name,ShouldEqual,"abc")
				So(ops[0].Value,ShouldNotBeNil)
				v,ok := ops[0].Value.(string)
				So(ok,ShouldBeTrue)
				So(v,ShouldEqual,"foo")
			})

			Convey("second option",func() {
				So(ops[1].Type,ShouldEqual,"int")
				So(ops[1].Name,ShouldEqual,"num")
				So(ops[1].Value,ShouldNotBeNil)
				v,ok := ops[1].Value.(int)
				So(ok,ShouldBeTrue)
				So(v,ShouldEqual,101)
			})

			Convey("third option",func() {
				So(ops[2].Type,ShouldEqual,"float")
				So(ops[2].Name,ShouldEqual,"f")
				So(ops[2].Value,ShouldNotBeNil)
				v,ok := ops[2].Value.(float64)
				So(ok,ShouldBeTrue)
				So(v,ShouldEqual,1.234)
			})

			Convey("Options.Find",func() {
				vs := Options(ops).Find("f")
				So(vs,ShouldNotBeEmpty)
				So(len(vs),ShouldEqual,1)
				So(vs[0].Type,ShouldEqual,"float")
				So(vs[0].Name,ShouldEqual,"f")
			})

			Convey("Options.String",func() {
				So(Options(ops).String("abc","bar"),ShouldEqual,"foo")
			})

			Convey("Options.Int",func() {
				So(Options(ops).Int("num",-1),ShouldEqual,101)
			})

			Convey("Options.Float",func() {
				So(Options(ops).Float("f",0.1),ShouldEqual,1.234)
			})
				
		})

	})
}


func Benchmark_Options(b *testing.B) {


	for i := 0; i < b.N; i++ {
		_,err := Parse("string abc","foo","int num",101,"float f",1.234)
		if err != nil {
			b.Fatal(err)
		}
	}
}



