/* mae.earth/pkg/options.go */
package options

import (
	"fmt"
	"strings"
	"errors"
)

var (
	ErrMismatched error = errors.New("Mismatched")
)


/* Option */
type Option struct {
	Type string
	Name string

	Value interface{}
}

/* Options */
type Options []Option 

/* Find */
func (o Options) Find(name string) []Option {

	/* do a quick count */
	if len(o) == 0 {
		return nil
	}

	out := make([]Option,0)

	for _,op := range o {
		if op.Name == name {
			out = append(out,Option{Type:op.Type,Name:op.Name,Value:op.Value})
		}
	}

	return out
}

/* String */
func (o Options) String(name,def string) string {

	/* cascade all by type and name */
	out := def

	for _,op := range o {
		if op.Type == "string" && op.Name == name {
			if v,ok := op.Value.(string); ok {
				out = v
			}
		}
	}

	return out
}

/* Int */
func (o Options) Int(name string,def int) int {

	out := def

	for _,op := range o {
		if op.Type == "int" && op.Name == name {
			if v,ok := op.Value.(int); ok {
				out = v
			}
		}
	}

	return out
}

/* Float */
func (o Options) Float(name string,def float64) float64 {

	out := def 

	for _,op := range o {
		if op.Type == "float" && op.Name == name {
			if v,ok := op.Value.(float64); ok {
				out = v
			}
		}
	}

	return out
}



/* Parse */
func Parse(parameterlist ...interface{}) ([]Option, error) {

	if len(parameterlist) % 2 != 0 {
		return nil,ErrMismatched
	}

	ops := make([]Option,0)

	token := ""
	for i, kv := range parameterlist {
		if i % 2 == 0 {
			if str,ok := kv.(string); ok {
				token = str
			} else {
				return nil,fmt.Errorf("bad token at position %d, was expecting string", i + 1)
			}
		} else {
			/* process the token */
			parts := strings.Split(token," ")
			if len(parts) != 2 {
				return nil,fmt.Errorf("bad token at position %d, was expecting %q", i, "type name")
			}

			ops = append(ops,Option{Type: parts[0], Name: parts[1], Value: kv})
		}
	}


	return ops,nil
}
